////////////////////////////////////////////////////////////////////////////////
//
// main.cpp
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <string>

#include "sorts/TSortBubble.h"
#include "sorts/TSortSelect.h"
#include "sorts/TSortInsert.h"
#include "sorts/TSortHeap.h"

#include "TSortingPerformanceTest.h"

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
    SortingTestDataSet theSortingData;

    TIndexType theSize = 1000;
    TSortingType *theTestArray = new TSortingType[theSize];
    for (TIndexType i = 0; i < theSize; ++i) theTestArray[i] = theSize - i;
    theSortingData.addTestArray("15, Reverse sorted", theTestArray, theSize);

    PerformanceTest<SortingTD<&bubbleSort> >("Bubble", &theSortingData);
    PerformanceTest<SortingTD<&selectSort> >("Select", &theSortingData);
    PerformanceTest<SortingTD<&insertSort> >("Insert", &theSortingData);
    PerformanceTest<SortingTD<&heapSort> >("Heap", &theSortingData);

    std::cout << "FINISHED!" << std::endl;

    return 0;
}
