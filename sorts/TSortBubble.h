////////////////////////////////////////////////////////////////////////////////
//
// TSortBubble.cpp
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORT_BUBBLE_H
#define T_SORT_BUBBLE_H

////////////////////////////////////////////////////////////////////////////////
#include "../TSortingCore.h"

////////////////////////////////////////////////////////////////////////////////
void bubbleSort(TSortingType *inoutArrayToSort, TIndexType inSize) {
    DECLARE_STATISTIC_DATA;

    PRINT_ARRAY(bubbleVerboseLevel, 1, inoutArrayToSort, inSize);

    int theTMP = 0;
    bool theSortIsFinished = false;

    for (TIndexType i = 0; i < inSize; ++i) {
        theSortIsFinished = true;
        for (TIndexType j = 0; j < inSize - 1; ++j) {
            if (ACCESS(inoutArrayToSort, j) > ACCESS(inoutArrayToSort, j + 1)) {
                SWAP(inoutArrayToSort[j], inoutArrayToSort[j + 1], theTMP);
                theSortIsFinished = false;
            }
        }

        PRINT_ARRAY(bubbleVerboseLevel, 1, inoutArrayToSort, inSize);

        if (theSortIsFinished) break;
    }

    PRINT_ARRAY(bubbleVerboseLevel, 1, inoutArrayToSort, inSize);
    PRINT_STATISTIC(bubbleVerboseLevel, 0);
}

////////////////////////////////////////////////////////////////////////////////
#endif // TSORT_BUBBLE_H
