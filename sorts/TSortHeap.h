////////////////////////////////////////////////////////////////////////////////
//
// TSortHeap.cpp
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORT_HEAP_H
#define T_SORT_HEAP_H

////////////////////////////////////////////////////////////////////////////////
#include "../TSortingCore.h"

////////////////////////////////////////////////////////////////////////////////
TIndexType powerOfTwo(TIndexType inPower) {
    return 1 << inPower;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TIndexType logTwo(TIndexType inValue) {
    //TODO: Verify if inValue == 0

    TIndexType theCounter = 1;
    inValue /= 2;

    while (inValue != 0) {
        inValue /= 2;
        ++theCounter;
    }
    return theCounter;
}

//------------------------------------------------------------------------------
TIndexType getElementIndex(TIndexType inDepthIndex, TIndexType inIndex) {
    // Verify index (if selected level has childs)
    const TIndexType kDepthOffset = powerOfTwo(inDepthIndex) - 1;
    return kDepthOffset + inIndex;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TIndexType getLevelSize(TIndexType inDepthIndex) {
    return powerOfTwo(inDepthIndex);
}

//==============================================================================
void heapSort(TSortingType *inoutArrayToSort, TIndexType inSize) {
    DECLARE_STATISTIC_DATA;

    PRINT_ARRAY(heapSortVerboseLevel, 1, inoutArrayToSort, inSize);

    TSortingType theTemporary = 0;

    for (TIndexType theHeapSize = inSize; theHeapSize != 0; --theHeapSize) {

        // Correct heap
        const TIndexType kDepth = logTwo(theHeapSize);

        if (kDepth < 2) {
            SWAP(inoutArrayToSort[0], inoutArrayToSort[theHeapSize - 1],
                    theTemporary);
            break;
        }

        TIndexType theDepthIndex = kDepth - 1;

        do {
            --theDepthIndex;

            const TIndexType theLevelSize = getLevelSize(theDepthIndex);
            for (TIndexType theLevelIndex = 0; theLevelIndex < theLevelSize;
                    ++theLevelIndex)
            {
                //TODO: Verify if such element is in array range!
                TSortingType theTemporary = 0;

                //(1)
                // {{{
                const TIndexType theElementIndex =
                        getElementIndex(theDepthIndex, theLevelIndex);
                if (theElementIndex >= theHeapSize) break;

                const TIndexType theLeftChildIndex =
                        getElementIndex(theDepthIndex + 1, theLevelIndex * 2);
                if (theLeftChildIndex >= theHeapSize) break;
                // }}}


                //(2)
                // {{{
                if (theLeftChildIndex + 1 == theHeapSize ||
                        ACCESS(inoutArrayToSort, theLeftChildIndex) >=
                                ACCESS(inoutArrayToSort, theLeftChildIndex + 1))
                {
                    if (ACCESS(inoutArrayToSort, theElementIndex) <
                            ACCESS(inoutArrayToSort, theLeftChildIndex))
                    {
                        SWAP(ACCESS(inoutArrayToSort, theElementIndex),
                                ACCESS(inoutArrayToSort, theLeftChildIndex),
                                theTemporary);
                    }
                } else {
                    if (ACCESS(inoutArrayToSort, theElementIndex) <
                                ACCESS(inoutArrayToSort, theLeftChildIndex + 1))
                    {
                        SWAP(ACCESS(inoutArrayToSort, theElementIndex),
                                ACCESS(inoutArrayToSort, theLeftChildIndex + 1),
                                theTemporary);
                    }
                }
                // }}}
            }
        } while (theDepthIndex != 0);

        PRINT_ARRAY(heapSortVerboseLevel, 1, inoutArrayToSort, inSize);

        SWAP(inoutArrayToSort[0], inoutArrayToSort[theHeapSize - 1],
                theTemporary);
    }

    PRINT_ARRAY(heapSortVerboseLevel, 1, inoutArrayToSort, inSize);
    PRINT_STATISTIC(heapSortVerboseLevel, 0);
}

////////////////////////////////////////////////////////////////////////////////
#endif // T_SORT_HEAP_H
