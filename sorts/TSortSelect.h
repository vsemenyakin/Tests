////////////////////////////////////////////////////////////////////////////////
//
// TSortSelect.cpp
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORT_SELECT_H
#define T_SORT_SELECT_H

////////////////////////////////////////////////////////////////////////////////
#include "../TSortingCore.h"

////////////////////////////////////////////////////////////////////////////////
void selectSort(TSortingType *inoutArrayToSort, TIndexType inSize) {

    DECLARE_STATISTIC_DATA;

    PRINT_ARRAY(selectVerboseLevel, 1, inoutArrayToSort, inSize);

    int theTMP = 0;

    int theMinIndex = 0;
    for (TIndexType i = 0; i < inSize; ++i) {
        theMinIndex = i;
        for (TIndexType j = i; j < inSize; ++j) {
            if (ACCESS(inoutArrayToSort, j) <
                    ACCESS(inoutArrayToSort, theMinIndex))
            {
                theMinIndex = j;
            }
        }
        SWAP(inoutArrayToSort[i], inoutArrayToSort[theMinIndex], theTMP);

        PRINT_ARRAY(selectVerboseLevel, 1, inoutArrayToSort, inSize);
    }

    PRINT_ARRAY(selectVerboseLevel, 1, inoutArrayToSort, inSize);
    PRINT_STATISTIC(selectVerboseLevel, 0);
}

////////////////////////////////////////////////////////////////////////////////
#endif // T_SORT_SELECT_H
