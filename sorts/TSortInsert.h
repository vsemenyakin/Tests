////////////////////////////////////////////////////////////////////////////////
//
// TSortInsert.cpp
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORT_INSERT_H
#define T_SORT_INSERT_H

////////////////////////////////////////////////////////////////////////////////
#include "../TSortingCore.h"

////////////////////////////////////////////////////////////////////////////////
void insertSort(TSortingType inoutArrayToSort[], TIndexType inSize) {
    DECLARE_STATISTIC_DATA;

    PRINT_ARRAY(insertVerboseLevel, 1, inoutArrayToSort, inSize);

    int theTMP = 0;

    for (TIndexType i = 1; i < inSize; ++i) {
        while(i != 0 &&
              ACCESS(inoutArrayToSort, i) < ACCESS(inoutArrayToSort, i - 1))
        {
            SWAP(inoutArrayToSort[i], inoutArrayToSort[i - 1], theTMP);
            --i;
        }
        PRINT_ARRAY(insertVerboseLevel, 1, inoutArrayToSort, inSize);
    }

    PRINT_ARRAY(insertVerboseLevel, 1, inoutArrayToSort, inSize);
    PRINT_STATISTIC(insertVerboseLevel, 0);
}

////////////////////////////////////////////////////////////////////////////////
#endif // T_SORT_INSERT_H
