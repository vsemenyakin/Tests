////////////////////////////////////////////////////////////////////////////////
//
// TPerformanceTestCore.h
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_PERFORMANCE_TEST_CORE_H
#define T_PERFORMANCE_TEST_CORE_H

////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <string>

#include <chrono>

#include "TCore.h"

////////////////////////////////////////////////////////////////////////////////
template<typename T_TestDefenition>
class PerformanceTest {
private:
    const TIndexType kTestCount = 1;

public:
    template<typename ... T_Arguments>
    PerformanceTest(T_Arguments ... inArguments) {
        TRealType theTestTime = 0.f;

        T_TestDefenition theTest = { inArguments ... };

        if (theTest.name().length() != 0) {
            std::cout << "=== " << theTest.name() << " ====" << std::endl;
        }

        if (!theTest.init()) return;

        do {
            if (theTest.caseName().length() != 0) {
                std::cout << "-- " << theTest.caseName() << " --" << std::endl;
            }

            theTestTime = 0.f;

            for (TIndexType theTestCount = 0; theTestCount < kTestCount;
                 ++theTestCount)
            {
                theTest.initCase();

                using namespace std::chrono;
                high_resolution_clock::time_point theBeginningTime =
                        high_resolution_clock::now();

                theTest.performCase();

                high_resolution_clock::time_point theEndingTime =
                        high_resolution_clock::now();

                theTestTime += duration_cast<milliseconds>(
                        theEndingTime - theBeginningTime).count();
            }
            std::cout << theTestTime / kTestCount << std::endl;

        } while(theTest.setNextTest());
    }
};

////////////////////////////////////////////////////////////////////////////////
#endif // TPERFORMANCE_TEST_CORE_H
