This is small project for different tests.

1. Sorting algorithms
    - Bubble sort
    - Select sort
    - Insert sort (currently not working)
    - Heap sort (has very low performance, should be optimized)

2. Project has classes infrastructure for utility tasks:
    - Unit tests and performance tests.
