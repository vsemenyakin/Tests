////////////////////////////////////////////////////////////////////////////////
//
// TSortingPerformanceTest.h
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORTING_PERFORMANCE_TEST_H
#define T_SORTING_PERFORMANCE_TEST_H

////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <string>

#include "TPerformanceTestCore.h"

////////////////////////////////////////////////////////////////////////////////
class SortingTestDataSet {
public:
    struct TestData {
        TestData(const std::string &inName,
                 const TSortingType *inData, TIndexType inSize)
            : name(inName), data(inData), size(inSize) { }

        std::string name;
        const TSortingType *data;
        TIndexType size;
    };

private:
    std::vector<TestData> _data;

public:
    ~SortingTestDataSet() {
        for (TIndexType theIndex = 0; theIndex < _data.size(); ++theIndex) {
            delete [] _data[theIndex].data;
        }
    }

    void addTestArray(const std::string &inName,
            const TSortingType *inArray, TIndexType inSize)
    {
        _data.push_back(TestData(inName, inArray, inSize));
    }

    const TestData &getTestData(TIndexType inIndex) const {
        return _data[inIndex];
    }

    TIndexType size() const { return _data.size(); }
};

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
template<SortingAlgorithPointer *T_SortingAlgorithPointer>
struct SortingTD {
private:
    //- State
    std::string _name;
    const SortingTestDataSet &_testDataSet;
    TIndexType _currentTestIndex;

    //-- Cache
    TSortingType *_testArray;
    TIndexType _testSize;

public:
    SortingTD(const std::string &inName,
            const SortingTestDataSet *inTestDataSet)
        : _name(inName), _testDataSet(*inTestDataSet), _currentTestIndex(0),
          _testArray(NULL), _testSize(0) { }

    ~SortingTD() {
        delete [] _testArray;
    }

    const std::string &name() { return _name; }
    const std::string &caseName() {
        return _testDataSet.getTestData(_currentTestIndex).name;
    }

    //-- Test management
    bool init() { return true; }

    bool setNextTest() {
        ++_currentTestIndex;
        return _currentTestIndex < _testDataSet.size();
    }

    //-- Case management
    void initCase() {
        const SortingTestDataSet::TestData &kTestData =
                _testDataSet.getTestData(_currentTestIndex);
        if (_testSize < kTestData.size) {
            if (_testArray) delete [] _testArray;
            _testArray = new TSortingType[kTestData.size];
        }

        _testSize = kTestData.size;
        memcpy(_testArray, kTestData.data, _testSize * sizeof(TSortingType));
    }

    void performCase() {
        T_SortingAlgorithPointer(_testArray, _testSize);
    }
};

////////////////////////////////////////////////////////////////////////////////
#endif // T_SORTING_PERFORMANCE_TEST_H
