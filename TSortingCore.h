////////////////////////////////////////////////////////////////////////////////
//
// TSortingCore.h
//
// Copyright (c) 2016 Semenyakin Vladimir (semenyakinVS@gmail.com)
//
// Distributed under the MIT (See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef T_SORTING_CORE_H
#define T_SORTING_CORE_H

////////////////////////////////////////////////////////////////////////////////
#include "TCore.h"

////////////////////////////////////////////////////////////////////////////////
#define ENABLE_DETAILS

#ifdef ENABLE_DETAILS
static const TIndexType bubbleVerboseLevel = 1;
static const TIndexType selectVerboseLevel = 1;
static const TIndexType insertVerboseLevel = 1;
static const TIndexType heapSortVerboseLevel = 1;
#endif // ENABLE_DETAILS

////////////////////////////////////////////////////////////////////////////////
typedef int TSortingType;

typedef void (SortingAlgorithPointer)(TSortingType *, TIndexType inSize);

////////////////////////////////////////////////////////////////////////////////

#define SWAP_IMPL(M_X, M_Y, M_Temporary)\
{\
    M_Temporary = M_X;\
    M_X = M_Y;\
    M_Y = M_Temporary;\
}

#ifdef ENABLE_DETAILS
#   define DECLARE_STATISTIC_DATA\
    SortingStatistic theSortingStatistic;

#   define PRINT_ARRAY(M_VerboseName, M_VerboseLevel, M_ArrayName, M_ArraySize)\
    if (M_VerboseName > M_VerboseLevel) printArray(M_ArrayName, M_ArraySize);

#   define ACCESS(M_Array, M_Index)\
    theSortingStatistic.access(M_Array, M_Index)

#   define SWAP(M_X, M_Y, M_Temporary)\
    theSortingStatistic.swap(M_X, M_Y, M_Temporary)

#   define PRINT_STATISTIC(M_VerboseName, M_VerboseLevel)\
    if (M_VerboseName > M_VerboseLevel) theSortingStatistic.print();

#else // ENABLE_DETAILS
#   define DECLARE_STATISTIC_DATA
#   define PRINT_ARRAY(M_VerboseName, M_VerboseLevel, M_ArrayName, M_ArraySize)
#   define ACCESS(M_Array, M_Index) M_Array[M_Index]
#   define SWAP(M_X, M_Y, M_Temporary) SWAP_IMPL(M_X, M_Y, M_Temporary)
#   define PRINT_STATISTIC(M_VerboseName, M_VerboseLevel)
#endif // ENABLE_DETAILS

struct SortingStatistic {
    TIndexType _lastAccessIndex;

    TIndexType _swapCount;
    TIndexType _acessCount;

    TIndexType _summeryAccessDistance;

    TIndexType distance(TIndexType inA, TIndexType inB) {
        return (inA >= inB) ? (inA - inB) : (inB - inA);
    }

public:
    SortingStatistic()
        : _lastAccessIndex(0), _swapCount(0), _acessCount(0),
          _summeryAccessDistance(0) { }

    void swap(TSortingType &inA, TSortingType &inB, TSortingType &inTemporary) {
        SWAP_IMPL(inA, inB, inTemporary);
        ++_swapCount;
    }

    TSortingType &access(TSortingType *inArray, TIndexType inIndex) {
        ++_acessCount;

        _summeryAccessDistance += distance(inIndex, _lastAccessIndex);
        _lastAccessIndex = inIndex;
        return inArray[inIndex];
    }

    void print() {
        std::cout << "Swaps count: " << _swapCount << std::endl;
        std::cout << "Access count: " << _acessCount << std::endl;
        std::cout << "Summery access distance: " <<
                     _summeryAccessDistance << std::endl;
        std::cout << "Awarage access distance: " <<
                     _summeryAccessDistance / _acessCount << std::endl;
    }
};

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void printArray(const TSortingType *inoutArrayToSort, TIndexType inSize) {
    std::cout << inoutArrayToSort[0];
    for (TIndexType i = 1; i < inSize; ++i) {
        std::cout << ", " << inoutArrayToSort[i];
    }
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
#endif // T_SORTING_CORE_H
